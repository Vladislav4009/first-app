package by.shag.gritskevich.api.dto.controller;

import by.shag.gritskevich.api.dto.model.ActorDto;
import by.shag.gritskevich.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ActorController {

    @Autowired
    private ActorService actorService;

    @GetMapping(value = "/actors")
    public String findAll(Model model) {
        model.addAttribute("actors", actorService.findAll());
        return "actors.html";
    }

    @GetMapping(value = "/new-actor")
    public String getCreateForm(Model model) {
        model.addAttribute("actor", new ActorDto());
        return "new-actor.html";
    }

    @PostMapping(value = "/new-actor")
    public String createNewActor(Model model, @ModelAttribute("actor") ActorDto actorDto) {
        actorService.saveActor(actorDto);
        return "redirect:/actors";
    }
    @DeleteMapping(value = "/delete")
    public String deleteActor(@RequestParam(value = "id") Integer id) {
        actorService.deleteById(id);
        return "redirect:/actors";
    }

    @GetMapping(value = "/find-actor")
    public String findActor(Model model) {
        model.addAttribute("actor", new ActorDto());
        return "find.html";
    }

    @GetMapping(value = "/find-actor-by-id")
    public String findActorById(Model model,
            @RequestParam(value = "id") Integer id) {
        model.addAttribute("actors", actorService.findById(id));
        return "get-actor.html";
    }

    @GetMapping(value = "/find-actor-by-name")
    public String findActorByNameAndLastName(Model model,
                                             @RequestParam(value = "name") String name,
                                             @RequestParam(value = "lastName") String lastName) {
        model.addAttribute("actors", actorService.findByNameAndLastName(name, lastName));
        return "get-actor.html";
    }

}
