package by.shag.gritskevich.service;

import by.shag.gritskevich.api.dto.model.ActorDto;
import by.shag.gritskevich.exception.EntityRepositoryException;
import by.shag.gritskevich.jpa.model.Actor;
import by.shag.gritskevich.jpa.repository.ActorRepositoryImpl;
import by.shag.gritskevich.jpa.transaction.EntityTransaction;
import by.shag.gritskevich.mapping.ActorMapper;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@Service
public class ActorService {

    private EntityTransaction entityTransaction = new EntityTransaction();
    private ActorMapper actorMapper = new ActorMapper();
    private Connection connection = entityTransaction.createConnection();
    private ActorRepositoryImpl actorRepository = new ActorRepositoryImpl(connection);

    public List<Actor> findAll() {
        List<Actor> actorList = new ArrayList<>();
        try {
            actorList = actorRepository.findAll();
        } catch (EntityRepositoryException e) {
            e.printStackTrace();
        }
        return actorList;
    }

    public void saveActor(ActorDto actorDto) {
        try {
            entityTransaction.startTransaction();
            actorRepository.save(actorMapper.convertFromDto(actorDto)
            );
            entityTransaction.commit();
        } catch (EntityRepositoryException e) {
            try {
                entityTransaction.rollback();
            } catch (EntityRepositoryException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                entityTransaction.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
    }

    public ActorDto findById(Integer id) {
        Actor actor = new Actor();
        try {
            actor = actorRepository.findById(id).orElseThrow(NullPointerException::new);
        } catch (EntityRepositoryException e) {
            e.printStackTrace();
        }
        ActorDto actorDto = actorMapper.convertToDto(actor);
        return actorDto;
    }

    public void deleteById(Integer id) {
        try {
            actorRepository.deleteById(id);
        } catch (EntityRepositoryException e) {
            e.printStackTrace();
        }
    }

    public void updateById(Actor actor, Integer id) {
        try {
            actorRepository.updateById(actor, id);
        } catch (EntityRepositoryException e) {
            e.printStackTrace();
        }
    }

    public Actor findByNameAndLastName(String name, String lastName) {
        Actor actor = new Actor();
        try {
            actor = actorRepository.findByNameAndLastName(name, lastName).orElseThrow(NullPointerException::new);
        } catch (EntityRepositoryException e) {
            e.printStackTrace();
        }
        return actor;
    }

    public void deleteByNameAndLastName(String name, String lastName) {
        try {
            actorRepository.deleteByNameAndLastName(name, lastName);
        } catch (EntityRepositoryException e) {
            e.printStackTrace();
        }
    }

}
