package by.shag.gritskevich.mapping;

import by.shag.gritskevich.api.dto.model.ActorDto;
import by.shag.gritskevich.exception.EntityRepositoryException;
import by.shag.gritskevich.jpa.model.Actor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ActorMapper implements EntityMapper<Actor> {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String LAST_NAME = "last_name";
    private static final String DATE_OF_BIRTH = "date_of_birth";
    private static final String SEX = "sex";
    private static final String COUNTRY = "country";

    @Override
    public Actor map(ResultSet resultSet) throws SQLException {
        Actor actor = new Actor();
        actor.setId(resultSet.getInt(ID));
        actor.setName(resultSet.getString(NAME));
        actor.setLastName(resultSet.getString(LAST_NAME));
        actor.setDateOfBirth(resultSet.getDate(DATE_OF_BIRTH));
        actor.setSex(resultSet.getInt(SEX) == 1 ? "man" : "woman");
        actor.setCountry(resultSet.getInt(COUNTRY) == 1 ? "USA" :
                resultSet.getInt(COUNTRY) == 2 ? "Great Britain" : "Australia");
        return actor;
    }

    @Override
    public void populate(Actor actor, PreparedStatement statement) throws EntityRepositoryException {
        try {
            statement.setString(1, actor.getName());
            statement.setString(2, actor.getLastName());
            statement.setDate(3, actor.getDateOfBirth());
            statement.setInt(4, actor.getSex().equals("man") ? 1 : 2);
            statement.setInt(5, 3);
            System.out.println("FUCK");
        } catch (SQLException e) {
            throw new EntityRepositoryException("Error in setString method", e);
        }
    }

    public Integer getCountryId(String country) throws EntityRepositoryException {
        switch (country) {
            case "USA":
                return 1;
            case "Great Britain":
                return 2;
            case "Australia":
                return 3;
            default:
//                throw new EntityRepositoryException("country not found");
                return 3;
        }
    }

    public Actor convertFromDto(ActorDto actorDto) {
        Actor actor = new Actor();
        actor.setId(actorDto.getId());
        actor.setName(actorDto.getName());
        actor.setLastName(actorDto.getLastName());
        actor.setDateOfBirth(actorDto.getDateOfBirth());
        actor.setSex(actorDto.getSex());
        actor.setCountry(actor.getCountry());
        return actor;
    }

    public ActorDto convertToDto(Actor actor) {
        ActorDto actorDto = new ActorDto();
        actorDto.setId(actor.getId());
        actorDto.setName(actor.getName());
        actorDto.setLastName(actor.getLastName());
        actorDto.setDateOfBirth(actor.getDateOfBirth());
        actorDto.setSex(actor.getSex());
        actorDto.setCountry(actor.getCountry());
        return actorDto;
    }
}
