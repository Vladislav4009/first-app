package by.shag.gritskevich.jpa.repository;

import by.shag.gritskevich.exception.EntityRepositoryException;

import java.util.Optional;

public interface CRUDRepository<E, ID> {

    // C - create
    public E save(E entity) throws EntityRepositoryException;

    // R - read
    public Optional<E> findById(ID id) throws EntityRepositoryException;

    //U - update
    public void updateById(E entity, ID id) throws EntityRepositoryException;

    // D - delete
    public void deleteById(ID id) throws EntityRepositoryException;

}
