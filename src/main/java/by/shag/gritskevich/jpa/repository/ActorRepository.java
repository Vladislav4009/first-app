package by.shag.gritskevich.jpa.repository;

import by.shag.gritskevich.exception.EntityRepositoryException;
import by.shag.gritskevich.jpa.model.Actor;

import java.util.Optional;

public interface ActorRepository extends CRUDRepository<Actor, Integer> {

    public Optional<Actor> findByNameAndLastName(String name, String lastName) throws EntityRepositoryException;

    public void deleteByNameAndLastName(String name, String lastName) throws EntityRepositoryException;

}
