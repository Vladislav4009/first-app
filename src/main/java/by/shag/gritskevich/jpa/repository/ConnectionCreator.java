package by.shag.gritskevich.jpa.repository;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class ConnectionCreator {

    public static Connection createConnection() {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(generateFilePath("src/main/resources/database.properties")));
        } catch (IOException e) {
            throw new RuntimeException("Can't found database.properties");
        }
        String databaseURL = (String) properties.get("db.url");
        try {
            return DriverManager.getConnection(databaseURL, properties);
        } catch (SQLException e) {
            throw new RuntimeException("Can't create connection to database", e);
        }
    }

    public static String generateFilePath(String fileName) {
        String dirName = System.getProperty("user.dir");
        return dirName + File.separator + fileName;
    }
}
