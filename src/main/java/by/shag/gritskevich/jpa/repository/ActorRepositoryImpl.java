package by.shag.gritskevich.jpa.repository;

import by.shag.gritskevich.exception.EntityRepositoryException;
import by.shag.gritskevich.jpa.model.Actor;
import by.shag.gritskevich.mapping.ActorMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ActorRepositoryImpl implements ActorRepository {

    private static final String SAVE =
            "insert into actors (name, last_name, date_of_birth, sex, country) values (?, ?, ?, ?, ?) ";
    private static final String FIND_BY_ID =
            "select id, name, last_name, date_of_birth, sex, country from actors a where a.id = ? ";
    private static final String DELETE_BY_ID =
            "delete from actors a where a.id = ? ";
    private static final String UPDATE_BY_ID =
            "update actors set name = ?, last_name = ?, date_of_birth = ?, sex = ?, country = ? where id = ? ";
    private static final String FIND_BY_NAME_AND_LASTNAME =
            "select id, name, last_name, date_of_birth, sex, country from actors where name = ? and last_name = ? ";
    private static final String DELETE_BY_NAME_AND_LASTNAME =
            "delete from actors a where a.name = ? and a.last_name = ? ";
    private static final String FIND_ALL =
            "select * from actors ";

    private Connection connection;
    private ActorMapper actorMapper = new ActorMapper();

    public ActorRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Actor save(Actor actor) throws EntityRepositoryException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SAVE, Statement.RETURN_GENERATED_KEYS)) {
            try {
                actorMapper.populate(actor, preparedStatement);
            } catch (EntityRepositoryException e) {
                throw new EntityRepositoryException("Exception in populate method", e);
            }
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            while (resultSet.next()) {
                actor.setId(resultSet.getInt("id"));
            }
            return actor;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Exception in save method", e);
        }
    }

    @Override
    public Optional<Actor> findById(Integer id) throws EntityRepositoryException {
        Actor actor = new Actor();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                actor = actorMapper.map(resultSet);
            }
        } catch (SQLException e) {
            throw new EntityRepositoryException("Actor not found", e);
        }
        return Optional.ofNullable(actor);
    }

    @Override
    public Optional<Actor> findByNameAndLastName(String name, String lastName) throws EntityRepositoryException {
        Actor actor = new Actor();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_NAME_AND_LASTNAME)) {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, lastName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                actor = actorMapper.map(resultSet);
            }
        } catch (SQLException e) {
            throw new EntityRepositoryException("Actor not found", e);
        }
        return Optional.ofNullable(actor);
    }


    @Override
    public void updateById(Actor actor, Integer id) throws EntityRepositoryException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BY_ID)) {
            actorMapper.populate(actor, preparedStatement);
            preparedStatement.setInt(6, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new EntityRepositoryException("Exception in update method", e);
        }
    }

    @Override
    public void deleteById(Integer id) throws EntityRepositoryException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID)){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new EntityRepositoryException("Exception in delete method", e);
        }
    }

    @Override
    public void deleteByNameAndLastName(String name, String lastName) throws EntityRepositoryException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_NAME_AND_LASTNAME)){
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, lastName);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new EntityRepositoryException("Exception in delete method", e);
        }
    }

    public List<Actor> findAll() throws EntityRepositoryException {
        List<Actor> actorList = new ArrayList<>();
        try (Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                actorList.add(actorMapper.map(resultSet));
            }
        } catch (SQLException e) {
            throw new EntityRepositoryException("Exception in findAll method", e);
        }
        return actorList;
    }

}
