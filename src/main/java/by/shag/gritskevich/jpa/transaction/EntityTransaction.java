package by.shag.gritskevich.jpa.transaction;

import by.shag.gritskevich.exception.EntityRepositoryException;
import by.shag.gritskevich.jpa.repository.ConnectionCreator;

import java.sql.Connection;
import java.sql.SQLException;

public class EntityTransaction {

    private Connection connection;

    public EntityTransaction() {
    }

    public Connection createConnection() {
        if (connection == null) {
            connection = ConnectionCreator.createConnection();
        }
        return connection;
    }

    public void startTransaction() throws EntityRepositoryException {
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new EntityRepositoryException("AutoCommit does not set to false");
        }
    }

    public void endTransaction() throws EntityRepositoryException {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new EntityRepositoryException("AutoCommit does not set to true", e);
        }
    }

    public void commit() throws EntityRepositoryException {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new EntityRepositoryException("Impossible to commit", e);
        }
    }

    public void rollback() throws EntityRepositoryException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new EntityRepositoryException("Impossible to rollback", e);
        }
    }
}
