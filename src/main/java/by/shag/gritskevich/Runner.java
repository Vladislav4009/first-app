package by.shag.gritskevich;

import by.shag.gritskevich.exception.EntityRepositoryException;
import by.shag.gritskevich.service.ActorService;
import by.shag.gritskevich.api.dto.model.ActorDto;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import java.sql.*;

@SpringBootApplication
@PropertySource("database.properties")
public class Runner {
    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }
    
    
    
//public static void main(String[] args) throws EntityRepositoryException {
//    ActorDto actorDto = new ActorDto();
//    actorDto.setName("Владислав4");
//    actorDto.setLastName("Грицкевич4");
//    actorDto.setDateOfBirth(Date.valueOf("1996-05-10"));
//    actorDto.setSex("man");
//    actorDto.setCountry("USA");

//    ActorService actorService = new ActorService();
////    actorService.saveActor(actorDTO);
////    actorDto = actorService.findById(5);
////    System.out.println(actorDto);
////    actorService.deleteById(19);
////    actorService.updateById(actorDto, 21);
////    actorDto = actorService.findByNameAndLastName("Matt", "LeBlanc");
////    System.out.println(actorDto);
//    actorService.deleteByNameAndLastName("Matt2", "LeBlanc");
//}
}
