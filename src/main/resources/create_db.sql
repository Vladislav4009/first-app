create table actors(
id serial primary key,
name varchar(30) not null,
last_name varchar(30) not null,
date_of_birth date not null,
sex Integer not null,
country Integer not null,
constraint fk_actors_country foreign key (country) references actors_country(country_id),
constraint fk_actors_sex foreign key (sex) references actors_sex(sex_id)
);

create table actors_country(
country_id serial primary key,
name varchar(30)
);

create table actors_sex(
sex_id serial primary key,
name varchar(20)
);

create table actorss(
id serial primary key,
name varchar(30) not null,
last_name varchar(30) not null
);

insert into actorss (name, last_name) values ('Morgan', 'Freeman');

insert into actors_sex (name) values ('man'); 
insert into actors_sex (name) values ('woman');
insert into actors_country (name) values ('USA');
insert into actors_country (name) values ('Great Britain');
insert into actors_country (name) values ('Australia');
insert into actors (name, last_name, date_of_birth, sex, country) values ('Morgan', 'Freeman', '06.01.1937', 1, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Tom', 'Hanks', '07.09.1956', 1, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('David', 'Morse', '10.11.1953', 1, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Michael', 'Duncan', '12.10.1957', 1, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Elijah', 'Wood', '07.28.1981', 1, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Ian', 'McKellen', '05.25.1939', 1, 2);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Cate', 'Blanchett', '05.14.1969', 2, 3);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Anne', 'Hathaway', '11.12.1982', 2, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Matthew', 'McConaughey', '11.04.1969', 1, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Cara', 'Delevingne', '08.12.1992', 2, 2);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Jennifer', 'Aniston', '02.11.1969', 2, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Courteney', 'Cox', '06.15.1964', 2, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Lisa', 'Kudrow', '07.30.1963', 2, 1);
insert into actors (name, last_name, date_of_birth, sex, country) values ('Matt', 'LeBlanc', '07.25.1967', 1, 1);

select id, name, last_name, date_of_birth, sex, country from actors where id = 3;

delete from actors a where a.id = 20;

update actors set name = 'ff', last_name = 'ee', date_of_birth = '02.23.1957', sex = 1, country = 2 where id = 21;

select id, name, last_name, date_of_birth, sex, country from actors where name = 'Matt' and last_name = 'LeBlanc';

delete from actors a where a.name = 'Владислав4' and a.last_name = 'Грицкевич4';


